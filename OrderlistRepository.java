package com.example.demo.persistance;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.entity.Orderlist;

public interface OrderlistRepository extends JpaRepository<Orderlist, Integer> {

}
