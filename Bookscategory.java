package com.example.demo.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity //defines structure of table
@Table(name="bookscategory") //it tells regarding table name
public class Bookscategory
{
	@Id //it informs regarding primary key
	@GeneratedValue(strategy = GenerationType.IDENTITY)  //tells regarding auto_increment
	@Column(name="categoryid") //it tells regarding column name
	private int categoryId;
	
	@Column(name="categoryname")
	private String categoryName;
	
	@Column(name="cunits")
	private String cUnits;

	public Bookscategory() {
		
	}

	public Bookscategory(int categoryId, String categoryName, String cUnits) {
		super();
		this.categoryId = categoryId;
		this.categoryName = categoryName;
		this.cUnits = cUnits;
	}

	@Override
	public String toString() {
		return "Bookscategory [categoryId=" + categoryId + ", categoryName=" + categoryName + ", cUnits=" + cUnits
				+ "]";
	}
	
	
	
}
	