package com.example.demo.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity //defines structure of table
@Table(name="orderlist") //it tells regarding table name
public class Orderlist
{
	@Id //it informs regarding primary key
	@GeneratedValue(strategy = GenerationType.IDENTITY)  //tells regarding auto_increment
	@Column(name="orderid") //it tells regarding column name
	private int orderId;
	
	@Column(name="userid")
	private Integer userId;
	
	@Column(name="username")
	private String userName;
	
	@Column(name="bookid")
	private Integer bookId;
	
	@Column(name="bookname")
	private int bookName;
	
	@Column(name="bookcategory")
	private String bookcategory;
	
	@Column(name="date")
	private String date;
	
	@Column(name="bill")
	private double bill;

	public Orderlist() {
		
	}

	public Orderlist(int orderId, Integer userId, String userName, Integer bookId, int bookName, String bookcategory,
			String date, double bill) {
		super();
		this.orderId = orderId;
		this.userId = userId;
		this.userName = userName;
		this.bookId = bookId;
		this.bookName = bookName;
		this.bookcategory = bookcategory;
		this.date = date;
		this.bill = bill;
	}

	public int getOrderId() {
		return orderId;
	}

	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Integer getBookId() {
		return bookId;
	}

	public void setBookId(Integer bookId) {
		this.bookId = bookId;
	}

	public int getBookName() {
		return bookName;
	}

	public void setBookName(int bookName) {
		this.bookName = bookName;
	}

	public String getBookcategory() {
		return bookcategory;
	}

	public void setBookcategory(String bookcategory) {
		this.bookcategory = bookcategory;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public double getBill() {
		return bill;
	}

	public void setBill(double bill) {
		this.bill = bill;
	}

	@Override
	public String toString() {
		return "Orderlist [orderId=" + orderId + ", userId=" + userId + ", userName=" + userName + ", bookId=" + bookId
				+ ", bookName=" + bookName + ", bookcategory=" + bookcategory + ", date=" + date + ", bill=" + bill
				+ "]";
	}

	
	
}
	
	
	