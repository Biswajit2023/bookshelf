package com.example.demo.service;

import java.util.List;

import com.example.demo.entity.Books;
import com.example.demo.entity.Orderlist;
import com.example.demo.entity.User;

public interface ServiceDeclaration {
	
	public List<Books> displayAllBooks();
	
	public List<User> getAllUsers();

	public void insertBooks(Books b);
	
	public void insertUser(User u);
	
	public void deleteBookById(int bookid);	
	
	public void updateBook(Books b);

	public void deleteUser(int userid);

		
	public List<Books> searchByBookName(String bookname);
	
	public List<Books> searchByAuotherName(String auotherName);
	

	
	
	
	
	

}
