package com.example.demo.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Books;
import com.example.demo.entity.Orderlist;
import com.example.demo.entity.User;
import com.example.demo.persistance.BookshelfRepository;
import com.example.demo.persistance.OrderlistRepository;
import com.example.demo.persistance.UserRepository;

@Service
public class ServiceImplementation implements ServiceDeclaration {

	private BookshelfRepository bsr;
	private UserRepository ur;
	private OrderlistRepository olist;
	
	@Autowired
	public ServiceImplementation(BookshelfRepository bsr, UserRepository ur, OrderlistRepository olist) {
		this.bsr = bsr;
		this.ur = ur;
		this.olist=olist;
	}
	@Override
	@Transactional
	public List<Books> displayAllBooks() {
		return bsr.findAll();
	}

	@Override
	@Transactional
	public void insertBooks(Books b) {
          bsr.save(b);	
	}

	@Override
	@Transactional
	public void insertUser(User u) {
          ur.save(u);	
	}

	@Override
	@Transactional
	public void deleteBookById(int bookid) {
		bsr.deleteById(bookid);	
	}

	@Override
	@Transactional
	public void updateBook(Books b) {
		bsr.save(b);
		
	}

	@Override
	@Transactional
	public List<Books> searchByBookName(String bookname) {
		
		return bsr.findByBookNameContainingIgnoreCase(bookname);
	}

	@Override
	@Transactional
	public User deleteUserById(int userid) {
		 User user = ur.getReferenceById(userid);
		ur.deleteById(userid);
		return user ;
	
	@Override
	@Transactional
	public List<Books> searchByAuotherName(String auotherName) {
		
		return bsr.findByAuthorContainingIgnoreCase(auotherName);
	}

	@Override
	@Transactional
	public List<User> getAllUsers() {
		return ur.findAll();
	}

	
}
