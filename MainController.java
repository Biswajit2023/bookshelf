package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entity.Books;
import com.example.demo.entity.Orderlist;
import com.example.demo.entity.User;
import com.example.demo.service.ServiceImplementation;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/main") //localhost:8080/main/books
public class MainController {
	
	private ServiceImplementation svc;
    
	private byte[] bytes;

	@Autowired
	public MainController(ServiceImplementation svc) {
		this.svc = svc;
	}
	
	@GetMapping("/books")
	public List<Books> displayAllBooks()
    {
    	return svc.displayAllBooks();
    }

	@GetMapping("/users")
    public List<User> getAllUsers() {
        return svc.getAllUsers();
    }

     @PostMapping("/upload")
	public void uploadImage(@RequestParam("imageFile") MultipartFile file) throws IOException {
		this.bytes = file.getBytes();
	}

	
	
	@PostMapping("/addbooks")
	public void addBooks(@RequestBody Books b)
	{
		b.setPicByte(this.bytes);
		svc.insertBooks(b);
		this.bytes = null;
	
	@DeleteMapping("/books/{bookid}")
	public void deleteBookById(@PathVariable("bookid") int bookid)
	{
		svc.deleteBookById(bookid);
	}
	
	@PutMapping("/books")
	public void updateBook(@RequestBody Books b)
	{
		 svc.updateBook(b);	
	}
	
	@DeleteMapping("/users/{userid}")
	public void deleteUserId(@PathVariable("userid") int userid)
	{
		svc.deleteUser(userid);
	}

	@PostMapping("/adduser")
	public void addUser(@RequestBody User u)
	{
		 svc.insertUser(u);
	}

    @GetMapping("/searchbybookname/{bookname}")
	public List <Books> searchByBookName(@PathVariable("bookname") String bookname )
	{
		return svc.searchByBookName (bookname);
	}
	
	@GetMapping("/searchbyauthorname/{authorname}")
	public List<Books> searchByAuthorName (@PathVariable("authorname") String auotherName )
	{
		return svc.searchByAuotherName (auotherName );
	}
	
}
