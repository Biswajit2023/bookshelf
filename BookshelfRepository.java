package com.example.demo.persistance;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.entity.Books;


public interface BookshelfRepository extends JpaRepository<Books, Integer> {
	
List<Books> findByBookNameContainingIgnoreCase(String bookname);
	List<Books> findByAuthorContainingIgnoreCase(String authorName);

}
