package com.example.demo.entity;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity 
@Table (name = "user")
public class User {

	@Id //it informs regarding primary key
	@GeneratedValue(strategy = GenerationType.IDENTITY)  //tells regarding auto_increment
	@Column(name="userid") //it tells regarding column name
	private int userId;
	
	@Column(name = "username")
	private String userName ;
	
	@Column (name = "useremail")
	private String userEmail;
	
	@Column(name = "userpassword")
	private String userPassword ;
	
	@Column (name = "useraddress")
	private String userAddress ;
	
	@Column (name = "userregion")
	private String userRegion ;
	
	@Column (name = "usermobile")
	private java.math.BigDecimal  userMobile ;

	public User() {
		super();
		// TODO Auto-generated constructor stub
	}

	public User(int userId, String userName, String userEmail, String userPassword, String userAddress,
			String userRegion, BigDecimal userMobile) {
		super();
		this.userId = userId;
		this.userName = userName;
		this.userEmail = userEmail;
		this.userPassword = userPassword;
		this.userAddress = userAddress;
		this.userRegion = userRegion;
		this.userMobile = userMobile;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserEmail() {
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	public String getUserPassword() {
		return userPassword;
	}

	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}

	public String getUserAddress() {
		return userAddress;
	}

	public void setUserAddress(String userAddress) {
		this.userAddress = userAddress;
	}

	public String getUserRegion() {
		return userRegion;
	}

	public void setUserRegion(String userRegion) {
		this.userRegion = userRegion;
	}

	public java.math.BigDecimal getUserMobile() {
		return userMobile;
	}

	public void setUserMobile(java.math.BigDecimal userMobile) {
		this.userMobile = userMobile;
	}

	@Override
	public String toString() {
		return "User [userId=" + userId + ", userName=" + userName + ", userEmail=" + userEmail + ", userPassword="
				+ userPassword + ", userAddress=" + userAddress + ", userRegion=" + userRegion + ", userMobile="
				+ userMobile + "]";
	} 
	
	
}
