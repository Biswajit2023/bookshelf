package com.example.demo.entity;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity //defines structure of table
@Table(name="books") //it tells regarding table name
public class Books
{
	@Id //it informs regarding primary key
	@GeneratedValue(strategy = GenerationType.IDENTITY)  //tells regarding auto_increment
	@Column(name="bookid") //it tells regarding column name
	private int bookId;
	
	@Column(name="bookname")
	private String bookName;
	
	@Column(name="author")
	private String author;
	
	@Column(name="bookprice")
	private double bookprice;
	
	@Column(name="bunits")
	private int bUnits;
	
	@Column(name="categoryid")
	private Integer categoryId;

	@Column(name = "picbyte")
	 private byte[] picByte;

	public Books() {
		
	}

	public Books(int bookId, String bookName, String author, double bookprice, int bUnits, Integer categoryId) {
		super();
		this.bookId = bookId;
		this.bookName = bookName;
		this.author = author;
		this.bookprice = bookprice;
		this.bUnits = bUnits;
		this.categoryId = categoryId;
	}

	public int getBookId() {
		return bookId;
	}

	public void setBookId(int bookId) {
		this.bookId = bookId;
	}

	public String getBookName() {
		return bookName;
	}

	public void setBookName(String bookName) {
		this.bookName = bookName;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public double getBookprice() {
		return bookprice;
	}

	public void setBookprice(double bookprice) {
		this.bookprice = bookprice;
	}

	public int getbUnits() {
		return bUnits;
	}

	public void setbUnits(int bUnits) {
		this.bUnits = bUnits;
	}

	public Integer getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}

	public byte[] getPicByte() {
		return picByte;
	 }

	 public void setPicByte(byte[] picByte) {
	 	this.picByte = picByte;
	 } 

	@Override
	public String toString() {
		return "Books [bookId=" + bookId + ", bookName=" + bookName + ", author=" + author + ", bookprice=" + bookprice
				+ ", bUnits=" + bUnits + ", categoryId=" + categoryId + "]";
	}

	
}
